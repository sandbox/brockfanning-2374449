<?php
/**
 * @file
 * Example hooks for Block Infojack.
 */

/**
 * Hook to manage block placement/visibility/titles/roles in code.
 *
 * @return array A multidimensional array, keyed first by theme name, then by
 *   region. Each region contains sequential arrays of block info, each of which
 *   is an associative array containing at least: 'module' and 'delta'. Optional
 *   array elements are: 'title', 'pages', 'visibility', and 'roles'.
 *   - 'roles', if used, must be an array of role names.
 *   - 'visibility', if used, can be any of these constants:
 *       BLOCK_VISIBILITY_NOTLISTED, BLOCK_VISIBILITY_LISTED, BLOCK_VISIBILITY_PHP
 *   - 'pages', if used, is the same as would normally be put into the textarea
 *       on the block configuration form. Multiple paths should be separated by a
 *       new-line character. (ie, \n inside double quotes)
 *   - 'title', if used, can be any string or <none> to omit the title.
 */
function hook_block_infojack() {

  return array(

    // Theme
    'my_user_facing_theme' => array(

      // Region
      'my_header_region' => array(

        array(
          'module' => 'my_custom_module',
          'delta' => 'my_logo_block',
          'title' => '<none>',
        ),
        array(
          'module' => 'search',
          'delta' => 'form',
          'title' => '<none>',
        ),
      ),

      // Another region
      'content' => array(

        array(
          'module' => 'my_custom_module',
          'delta' => 'my_front_page_hero_block',
          'title' => t('Featured Story'),
          'pages' => '<front>',
          'visibility' => BLOCK_VISIBILITY_LISTED,
        ),
        array(
          'module' => 'system',
          'delta' => 'main',
        ),
        array(
          'module' => 'my_custom_module',
          'delta' => 'my_call_to_action_block',
          'title' => t('Sign Up For Our Newsletter!'),
          // Use double-quotes to get the new-line.
          'pages' => "articles/*\nvideos/*",)
      ),
    ),

    // Another theme
    'my_admin_theme' => array(

      'content' => array(

        array(
          'module' => 'my_custom_module',
          'delta' => 'my_editor_block',
          'title' => t('For Editors Only'),
          'roles' => array('editor'),
        ),
        array(
          'module' => 'system',
          'delta' => 'main',
        ),
      ),
    ),
  );
}
